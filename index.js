// create an empty modbus client
//~ var ModbusRTU = require("modbus-serial");
//~ var client = new ModbusRTU();

//~ // open connection to a tcp line
//~ client.connectTCP("192.168.1.2", { port: 502 });
//~ client.setID(1);

// read the values of 10 registers starting at address 0
// on device number 1. and log the values to the console.
//~ setTimeout(function() {
    //~ client.readInputRegisters(0, 10, function(err, data) {
        //~ console.log(err,data);
        //~ client.close();
    //~ });
//~ }, 1000);
//~ setTimeout(function() {
    //~ client.readInputRegisters(0, 10)
		//~ .then(results => {
			//~ console.log(results);
			//~ return results;
		//~ })
		//~ .catch(error => {
			//~ console.error(error);
			//~ return null;
		//~ })
//~ }, 1000);

const __modbus = require('./modbusRTU.class');
const config = {
		ip : "192.168.1.2",
		port : 502,
		id : 1
	};
const modbus = new __modbus(config);

(async () => {
	let i = 65530;
	let x = 9.8765432109876543210;
	setInterval(async function(){
		//~ console.log();
		//~ console.log('-------------');
		//~ const valWORDS = modbus.intToWords(i,"WORDS");
		//~ const valDWORDS = modbus.intToWords(i,"DWORDS");
		//~ console.log(i,valWORDS,valDWORDS);
		const write1 = await modbus.writeRegisters(0,modbus.intToWords(i++));
		const write2 = await modbus.writeRegisters(1,modbus.floatToWords(x));
		//~ i++;
		x += 0.00001;
		//const val1 = await modbus.readInputRegisters(0,2);
		//const val2 = await modbus.readInputRegisters(1);
		//const val3 = await modbus.readHoldingRegisters(2);
		//~ const valAll = await modbus.readHoldingRegisters(0,2);

		//~ console.log(Buffer.from(val1).readInt16BE(), Buffer.from(val2).readInt16BE(), Buffer.from(val3).readInt16BE());
		//~ console.log(valAll);
		//console.log(modbus.toInt(val1), modbus.toInt(val2), modbus.toInt(val3));
		//~ const valReal = await modbus.readInputRegisters(0);
		//~ const valReal2 = await modbus.readInputRegisters(1);
		//~ console.log(valReal.readInt16BE(), valReal2.readInt16BE(), valAll.readInt16BE());
	},1000);
})();
