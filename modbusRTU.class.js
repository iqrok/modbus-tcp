const EventEmitter = require('events');
const ModbusRTU = require("modbus-serial");

'use strict;'
class modbus extends EventEmitter {
	constructor( modbusConfig ) {
		super();
		const self = this;
		self.conf = {
				ip : modbusConfig.ip,
				port : modbusConfig.port,
				id : modbusConfig.id
			};

		self.client = new ModbusRTU();

		// changed opening client method from callback to promise
		self.openClient = function() {
			return new Promise(function(resolve, reject) {
				self.client.connectTCP(self.conf.ip, {port : self.conf.port}, function(err){
					if(self.client.isOpen){
						resolve(true);
						return;
					}

					//set id for first time connection
					self.client.setID(self.conf.id);

					if(err){
						console.log("client open Error",err);
						return reject(err);
					}

					return resolve(self.client.isOpen);
				});
			});
		}

	}

	readInputRegisters(addr,len = 1){
		const self = this;

		// return register value
		return self.openClient()
			.then(response => {
				return self.client.readInputRegisters(addr, len)
					.then(data => {
						return Buffer.from(data.buffer);
					})
					.catch(error => {
						console.error('client.readInputRegisters error :',addr,error);
						throw null;
					});
			})
			.catch(error => {
				console.error('readInputRegisters error :',addr,error);
				return null;
			})
			.finally(() => {
				self.client.close();
			});
	};

	readHoldingRegisters(addr,len = 1){
		const self = this;

		// return register value
		return self.openClient()
			.then(response => {
				return self.client.readHoldingRegisters(addr, len)
					.then(data => {
						return Buffer.from(data.buffer);
					})
					.catch(error => {
						console.error('client.readHoldingRegisters error :',addr,error);
						throw null;
					});
			})
			.catch(error => {
				console.error('readHoldingRegisters error :',addr,error);
				return null;
			})
			.finally(() => {
				self.client.close();
			});
	};

	writeRegister(addr,value){
		const self = this;

		// return register value
		return self.openClient()
			.then(response => {
				return self.client.writeRegister(addr, value)
					.then(data => {
						console.log(data);
						return true;
					})
					.catch(error => {
						console.error('client.writeRegisters error :',addr,error);
						throw false;
					});
			})
			.catch(error => {
				console.error('writeRegisters error :',addr,error);
				return false;
			})
			.finally(() => {
				self.client.close();
			});
	};

	writeRegisters(addr,value){
		const self = this;

		// return register value
		return self.openClient()
			.then(response => {
				return self.client.writeRegisters(addr, value)
					.then(data => {
						return true;
					})
					.catch(error => {
						console.error('client.writeRegisters error :',addr,error);
						throw false;
					});
			})
			.catch(error => {
				console.error('writeRegisters error :',addr,error);
				return false;
			})
			.finally(() => {
				self.client.close();
			});
	};

	toInt(__buffer, isBigEndian = true){
		const bytes = __buffer.length ? __buffer.length : null;

		if(!bytes)
			return __buffer;

		if(bytes == 4 || bytes == 2){
			return Buffer.from(__buffer).readInt16BE();
		}
		else if(bytes == 8){
			return Buffer.from(__buffer).readInt32BE();
		}
		else{
			return Buffer.from(__buffer);
		}
	};

	//~ toBytes(num, type = 'int16') {
		//~ let value = 0;
		//~ let arr,view;

		//~ switch(type){
			//~ case 'int16' :
				//~ arr = new ArrayBuffer(2); // an Int16 takes 2 bytes
				//~ view = new DataView(arr);
				//~ view.setUint16(0, num, false);
				//~ value = Buffer.from(arr).readInt16BE();
				//~ break;
			//~ case 'int32' :
				//~ arr = new ArrayBuffer(4); // an Int32 takes 4 bytes
				//~ view = new DataView(arr);
				//~ view.setUint32(0, num, false);
				//~ value = Buffer.from(arr).readInt32BE();
				//~ break;
			//~ case 'float' :
				//~ arr = new ArrayBuffer(4); // an float takes 4 bytes
				//~ view = new DataView(arr);
				//~ view.setFloat32(0, num, false);
				//~ const arrFloat = Buffer.from(arr);
				//~ value = arrFloat;
				//~ console.log(arrFloat[0]);
				//~ break;
		//~ }

		//~ return value;
	//~ };

	intToWords(num, type = 'WORDS') {
		const arr = new Uint8Array([
			 (num & 0x000000ff),
			 (num & 0x0000ff00) >> 8,
			 (num & 0x00ff0000) >> 16,
			 (num & 0xff000000) >> 24,
		]);

		const WORDS =[(arr[3] << 8 | arr[2]), (arr[1] << 8 | arr[0])];

		const tmp1 = arr.map(item => {
				return item.toString(16);
			});

		const tmp2 = WORDS.map(item => {
				return item.toString(16);
			});
		//~ console.log('bytes :',arr[3].toString(16),arr[2].toString(16),arr[1].toString(16),arr[0].toString(16));
		//~ console.log('words :',tmp2);
		return type == 'WORDS' ? WORDS.slice(1) : WORDS;
	};

	floatToWords(num){
		const buffer = new ArrayBuffer(4);
		const intView = new Int32Array(buffer);
		const floatView = new Float32Array(buffer);

		floatView[0] = num;

		// LE
		const arr = [
				(intView[0] >>  0) & 0xFF,
				(intView[0] >>  8) & 0xFF,
				(intView[0] >> 16) & 0xFF,
				(intView[0] >> 24) & 0xFF,
			];

		// BE
		//~ const arr = [
				//~ (intView[0] >> 24) & 0xFF,
				//~ (intView[0] >> 16) & 0xFF,
				//~ (intView[0] >>  8) & 0xFF,
				//~ (intView[0] >>  0) & 0xFF,
			//~ ];

		const WORDS =[(arr[3] << 8 | arr[2]), (arr[1] << 8 | arr[0])];

		/*********** DEBUG *********** /
		const tmp1 = arr.map(item => {
				return item.toString(16);
			});

		const tmp2 = WORDS.map(item => {
				return item.toString(16);
			});

		console.log(num, Buffer.from(arr).readFloatLE(),tmp1,tmp2);
		/*********** end of DEBUG ***********/

		return WORDS;
	};
}

module.exports = modbus;
